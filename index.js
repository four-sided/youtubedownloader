const fs = require('fs');
const ytdl = require('ytdl-core');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');
const metadata = require('ffmetadata');
const https = require("https");


const YTDownloader = class {


  /**
   * @class
   * @param {boolean} verbose - Give out debug information
   * @param {string} folderpath - The path to store the downloaded videos
   * @param {string} dbpath - The path to store the database
   */
  constructor(verbose = false, folderpath = "./output/", dbpath = "./database.json") {
    this.verbose = verbose;
    this.printDebug("Verbose mode turned on.");
    this.dbpath = dbpath;
    if(!fs.existsSync(dbpath)){
      this.db = {"songs": []};
      this.saveDatabase();
    } else {
      this.db = JSON.parse(fs.readFileSync(dbpath));
    }
    this.printDebug(`dbpath = ${dbpath}`);
    this.path = path.resolve(folderpath);
    this.printDebug(`folderpath = ${folderpath}:${this.path}`);
    if (!fs.existsSync(this.path))
      fs.mkdirSync(this.path);
  }


  /** (re-)loads the database
   */
   loadDatabase(){
     this.db = JSON.parse(fs.readFileSync(this.dbpath));
   }

  /** saves the database
   * @private
   */
   saveDatabase(){
     fs.writeFile(this.dbpath, JSON.stringify(this.db), 'utf8', err => {if(err){console.log(err)}});
   }



  /** Returns an object containing all songs keyed by id
   * @param {YTDownloader~GetAllSongsCallback} callback - Callback to execute on finish.
   */

  getSongs(callback = undefined) {
    let songs = this.db.songs;

    if(callback instanceof Function){
      if(err) callback(err, undefined);
      else callback(undefined, songs);
    } else throw err;
  }



  /**
   *  Returns a single song object if existing
   *  @param {string} id - the video id
   *  @param {YTDownloader~GetSongIdCallback} callback - err might be undefined if no error occured.  result is the returned song if found, otherwise undefined
   */

  getSongById(id, callback = undefined) {

    for (let i=0; i < this.db.songs.length; i++)
      if (this.db.songs[i]['id'] == id)
        return this.db.songs[i];
  }


  printDebug(text) {
    if (this.verbose)
      console.log(text);
  }

  /** Download a song
   * @param {string} url - the youtube url or video id
   * @param {boolean} audioOnly - should the file
   * @param {boolean} writeMeta - should the meta get written as well? Only works if audioOnly is true.
   * @param {YTDownloader~downloadCallback} callback - The callback to execute once the function is done or an error gets thrown
   */
  download(url, audioOnly = false, writeMeta = true, callback = undefined) {
    if (!ytdl.validateURL(url)) {
      if (ytdl.validateID(url)) {
        url = "https://youtu.be/" + url;
      }
    }

    this.printDebug(`Trying to Download: ${url}`);
    this.printDebug(`audioOnly: ${audioOnly}\nwriteMeta: ${writeMeta}`);

    ytdl.getInfo(url, (err, info) => {
      if(err)
        if(callback instanceof Function) callback(err, undefined);
        else throw err;

      let videoInfo = {
        "id": info.video_id,
        "title": info.title,
        "uploader": info.author,
        "keywords": info.keywords,
        "description": info.description,
        "thumbnail": info.player_response.videoDetails.thumbnail.thumbnails[info.player_response.videoDetails.thumbnail.thumbnails.length - 1],
        "published": info.published
      };

      let output = path.resolve(this.path, `${videoInfo.id}.mp3`)

      // The actual download
      let stream = ytdl.downloadFromInfo(info, {quality: audioOnly ? "highest" : "highestaudio"});
      let processor = stream;

      if(audioOnly){
        processor = ffmpeg(stream)
          .audioBitrate(128)
          .save(output)
      } else {
        processor = stream.pipe(fs.createWriteStream(output));
      }

      processor.on("end", () => {
        // Download Thumbnail
        let cover_location = path.resolve(this.path, "covers", `${videoInfo.id}.jpg`);
        videoInfo.thumbnail.file = cover_location;
        this.printDebug(`Downloading cover to: ${cover_location}`);
        let coverWriter = fs.createWriteStream(cover_location);
        https.get(videoInfo.thumbnail.url, (res) => {
          res.on("data", (chunk) => {
            coverWriter.write(chunk);
          });

          res.on("end", () => {
            coverWriter.end();
            this.printDebug("Cover download finished");

            this.db.songs.push(videoInfo);
            this.saveDatabase();

            if(audioOnly && writeMeta){
              if(callback instanceof Function)
                this._write_meta(videoInfo, callback);
              else
                return this._write_meta(videoInfo, false);
            } else {
              if(callback instanceof Function)
                callback(false, videoInfo);
            }
          });
        });
      });
    });
  }

  /**
   * Get Information of Song and Write Meta
   * @param {string} url - The url to get information for
   * @param {YTDownloader~Song} data - Not supposed to get used by other packages
   * @param {YTDownloader~downloadCallback} callback - The callback to run once finished.
   */
  putinfo(url, data = null, callback) {
    if (data == null && !ytdl.validateURL(url)) {
      if (ytdl.validateID(url)) {
        url = "https://youtu.be/" + url;
      }
    }

    let id = data == null ? ytdl.getURLVideoID(url) : data.id;
    this.printDebug(`Getting meta for ${id}`);

    //get Video info
    if (data == null)
      ytdl.getInfo(id, (err, info) => {
        if (err) {
          callback(err);
          return;
        }

        // Download Thumbnail
        let cover_location = path.resolve(this.path, "covers", `${id}.jpg`);
        this.printDebug(`Downloading cover to: ${cover_location}`);
        let coverWriter = fs.createWriteStream(cover_location);
         https.get(info.thumbnail_url, (res) => {
           res.on("data", (chunk) => {
             coverWriter.write(chunk);
           });

           res.on("end", () => {
             coverWriter.end();
             this.printDebug("-- Cover download finished --");
           })
         });

        this._write_meta(id, info);
      });
    else
      return this._write_meta(data.id, data, callback);
  }

  /** Actual Write Meta
  * @private
  */
  _write_meta(info, callback) {
    let output = path.resolve(this.path, `${info.id}.mp3`);
    this.printDebug(`Writing meta to: ${output}`);
    var meta = {
      artist: info.uploader.name,
      title: info.title,
      date: new Date(info.published).getUTCFullYear(),
      album: 'YoutubeDownloader',
      comment: 'Automatically generated by Youtubedownloader'
    };

    metadata.write(output, meta, {
      "attachments": [info.thumbnail.file],
      "id3v2.3": true
    }, (err) => {
      this.printDebug("Writing Meta finished.");
      if (err) {
        if(callback instanceof Function)
          callback(err, false);
        else throw err;
      }

      if (callback instanceof Function)
        callback(false, info);
      else {
        return info;
      }
    });
  }

}

function YTDL(verbose=false, folderpath="./output/", dbpath="database.db"){
  return new YoutubeDownloader(verbose, folderpath, dbpath);
}

module.exports = YTDownloader;

// Callback Descriptions

/** @callback YTDownloader~downloadCallback
* @param {error | undefined} err - If an error occurs, the callback gets called with this set.
* @version 1.2.0
* @param {YTDownloader~Song | undefined} - Returns the downloaded song
*/

/** @callback YTDownloader~GetSongIdCallback
* @param {error | undefined} err - If an error occurs, the callback gets called with this set.
* @param {YTDownloader~Song | undefined} result - If an song is found and no error occured this will be set.
*/

/** @callback YTDownloader~GetAllSongsCallback
* @param {error | undefined} err - If an error occurs, the callback gets called with this set.
* @param {...YTDownloader~Song | undefined} result - Is undefined if no songs exist.
*/

// Additional Objects

/**
* A Thumbnail Object
* Orignally adapted from ytdl-core with additions
* @typedef {Object} YTDownloader~Thumbnail
* @since 1.2.0
* @property {string} url - The thumbnail url
* @property {string} file - The absolute path to the local file
* @property {int} width - The width of the thumbnail
* @property {int} height - The height of the thumbnail
*/

/**
* An Uploader Object
* Orginally adapted from ytdl-core
* @typedef YTDownloader~Uploader
* @since 1.2.0
* @property {string} id - The id of the uploader
* @property {string} name - The current name of the uploader
* @property {string} avatar - The url of the avatar
* @property {boolean} verified - Boolean determining if the uploader is verified
* @property {string} user - Synonym for name
* @property {string} channel_url - The url to the channel determined by YouTube
* @property {string} user_url - User determined channel url
*/

/**
* A Song Object
* @typedef {Object} YTDownloader~Song
* @property {string} id - the video id
* @property {string} title - The title of the video
* @property {YTDownloader~Uploader} uploader - Object of the uploader
* @property {string[]} keywords - The keywords of the video
* @property {string} description - The description of the video
* @version 1.2.0
* @property {YTDownloader~Thumbnail} thumbnail - The description of the video
* @version 1.2.0
* @property {int} published - The published date in milliseconds
*/
